<?php
return [
    'adminEmail' => 'rahul.swami@ranosys.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];

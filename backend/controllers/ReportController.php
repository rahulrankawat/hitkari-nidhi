<?php

namespace backend\controllers;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\models\Donation;

class ReportController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = new Donation();
        return $this->render('index', ['model' => $model]);
    }

}

<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\Districts;
use backend\models\Employees;
use backend\models\Blocks;
use backend\models\Donation;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'ajax-get-blocks', 'ajax-get-employees'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        /* ---- Code to import employees data into system----- */
//        $emoployeeModel = new Employees();
//        $districtModel = new Districts();
//        $blockModel = new Blocks();
//        $inputFileName = Yii::getAlias('@uploads') . '/Book1.xlsx';
//        $inputFileType = 'Xlsx';
//
//        $reader = IOFactory::createReaderForFile($inputFileName);
//        $reader->setReadDataOnly(true);
//        $spreadsheet = $reader->load($inputFileName);
//        $maxRow = $spreadsheet->getActiveSheet()->getHighestDataRow();
//        if ($maxRow > 20000) {
//            exit('to many rows.');
//        }
//        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
//        $resultData = array();
//        unset($sheetData[1]);
//        $counter = 0;
//        foreach ($sheetData as $employeeData) {
//            if ($maxRow == $counter) {
//                break;
//            }
//            if (!empty($employeeData['M'])) {
//                $districtId = Districts::findOne(['district_name' => $employeeData['E']]);
//                $blockId = Blocks::findOne(['block_name' => $employeeData['F']]);
//                $resultData[$counter]['emp_id'] = $employeeData['M'];
//                $resultData[$counter]['emp_name'] = $employeeData['I'];
//                $resultData[$counter]['school_name'] = $employeeData['C'];
//                $resultData[$counter]['district'] = (isset($districtId['id']) ? $districtId['id'] : '');
//                $resultData[$counter]['block'] = (isset($blockId['id']) ? $blockId['id'] : '');
//                $resultData[$counter]['contact_number'] = $employeeData['N'];
//                $resultData[$counter]['employee_email'] = '';
//            }
//            $counter++;
//        }
//
//        Yii::$app->db->createCommand()->batchInsert('employees', ['emp_id', 'emp_name', 'school_name', 'district', 'block', 'contact_number', 'employee_email'], $resultData)->execute();
        /* ------ Import code END------------- */


        $district = new Districts();
        $block = new Blocks();
        $employess = new Employees();
        $Donation = new Donation();
        $postedEmployeeId = '';
        $postedDistrictId = '';
        $postedBlockId = '';
        if (Yii::$app->request->post()) {
            $postedDistrictId = Yii::$app->request->post('Districts')['id'];
            $postedBlockId = Yii::$app->request->post('Blocks')['id'];
            $postedEmployeeId = Yii::$app->request->post('Employees')['id'];
        }
        $dataProvider = $Donation->search(Yii::$app->request->queryParams, $postedEmployeeId, $postedDistrictId, $postedBlockId);
        return $this->render('index', ['district' => $district, 'block' => $block, 'employess' => $employess, 'dataProvider' => $dataProvider]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved successfully.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionAjaxGetBlocks() {
        if (Yii::$app->request->post('districtId') != '') {
            $result = Blocks::find()->where(['district_id' => Yii::$app->request->post('districtId')])->asArray()->all();
            $data['result'] = $result;
        } else {
            $data['result'] = "";
        }

        // return Json    
        return \yii\helpers\Json::encode($data);
    }

    public function actionAjaxGetEmployees() {
        if (Yii::$app->request->get('districtId') != '' && Yii::$app->request->get('blockId') != '') {
            $result = Employees::find()->where(['district' => Yii::$app->request->get('districtId')])->andWhere(['block' => Yii::$app->request->get('blockId')])->asArray()->all();
            $data['result'] = $result;
        } else {
            $data['result'] = "";
        }

        // return Json    
        return \yii\helpers\Json::encode($data);
    }

}

<?php

namespace backend\controllers;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\models\Donation;
use backend\models\Employees;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;

class DonationController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'preview', 'update', 'delete', 'view', 'send-email', 'download', 'ajax-get-employees'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = new Donation();
        $employeeArray = Employees::find()->select('emp_id')->asArray()->all();
        $employeeId = array();
        foreach ($employeeArray as $values) {
            $employeeId[] = $values['emp_id'];
        }
        if (!empty(Yii::$app->request->post())) {
            $model->load(Yii::$app->request->post());
            $model->contribution_year = date('Y', strtotime(Yii::$app->request->post('Donation')['contribution_year']));
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Donation added successfully.');
                $lastInsertedId = base64_encode($model->id);
                $this->redirect(['donation/preview', 'id' => $lastInsertedId]);
            } else
                Yii::$app->session->setFlash('error', 'Some error occured, please try agian.');
        }
        return $this->render('index', ['model' => $model, 'employeeArray' => $employeeId]);
    }

    public function actionPreview($id) {
        $donationID = base64_decode($id);
        $donationData = Donation::find()->where(['id' => $donationID])->one();
        return $this->render('preview', ['donation' => $donationData]);
    }

    public function actionUpdate($id) {
        $employeeArray = array('empty');
        $ddoArray = array('empty');
        $empNameArray = array('empty');
        $schoolArray = array('empty');
        $model = $this->findModel(base64_decode($id));
        if (!empty($model->load(Yii::$app->request->post()))) {
            $model->load(Yii::$app->request->post());
            $model->contribution_year = date('Y', strtotime(Yii::$app->request->post('Donation')['contribution_year']));
            if ($model->save(FALSE)) {
                Yii::$app->session->setFlash('success', 'Donation updated successfully.');
                $lastInsertedId = base64_encode($model->id);
                $this->redirect(['donation/preview', 'id' => $lastInsertedId]);
            } else {
                Yii::$app->session->setFlash('error', 'Some error occured, please try agian.');
            }
        }


        return $this->render('update', [
                    'model' => $model,
                    'employeeArray' => $employeeArray,
                    'ddoArray' => $ddoArray,
                    'empNameArray' => $empNameArray,
                    'schoolArray' => $schoolArray
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel(base64_decode($id));
        $model->mark_as_delete = 1;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Donation deleted successfully.');
            $this->redirect(['site/index']);
        } else {
            $lastInsertedId = base64_encode($id);
            Yii::$app->session->setFlash('error', 'Some error occured, please try agian.');
            $this->redirect(['donation/preview', 'id' => $lastInsertedId]);
        }
    }

    public function actionView($id) {
        return $this->render('view', [
                    'donation' => $this->findModel(base64_decode($id)),
        ]);
    }

    protected function findModel($id) {
        if (($model = donation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSendEmail($id) {
        $content = $this->renderPartial('_donationPDF', [
            'donation' => $this->findModel(base64_decode($id)),
        ]);

        $header = $this->renderPartial('_donationPdfHeader', [
            'donation' => $this->findModel(base64_decode($id)),
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => 'Krajee Report Title'],
            'methods' => [
                'SetHeader' => [$header],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        $mpdf = $pdf->api;
        $mpdf->SetHeader($header);
        $mpdf->SetFooter('{PAGENO}');
        $mpdf->WriteHtml($content);
        $mpdf->Output(Yii::getAlias('@backend') . '/uploads/temp_pdf/donation-' . base64_decode($id) . '.pdf', 'F');

        /* ------------- Send mail to employee with receipt attachment------------ */
        $donation_details = $this->findModel(base64_decode($id));
        $emailBody = '<html lang="en"><head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body style="margin:0; padding:0; background-color:#FFF;">
  <center>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFF">
    <tr>
    <td align="center" valign="top"><img style="width:10%;" src="' . Yii::$app->params['siteUrl'] . 'images/site-logo.png"/></td>
    </tr>
    </table>
  </center>
    <table width="50%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFF" style="padding: 30px;margin-top: 5%;">
    <tr>
    <tr>
            <td valign="top">Hello ' . $donation_details->emp_name . '</td>
        </tr>
        <td valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">Please find the attached donation receipt for contribution year ' . date('Y', strtotime($donation_details->contribution_year)) . '.</td>
        </tr>
        </table>
</body>
</html>';

        Yii::$app->mail->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo('rahulswami482@gmail.com')
                ->setSubject('Hitkari Nidhi - Donation Receipt - ' . date('Y', strtotime($donation_details->contribution_year)))
                ->setHtmlBody($emailBody)
                ->attach(Yii::getAlias('@backend') . '/uploads/temp_pdf/donation-' . base64_decode($id) . '.pdf')
                ->send();

        $donation_details->status = 1;
        unlink(Yii::getAlias('@backend') . '/uploads/temp_pdf/donation-' . base64_decode($id) . '.pdf');
        if ($donation_details->save(false)) {
            Yii::$app->session->setFlash('success', 'Donation receipt successfully sent to employee.');
            $this->redirect(['donation/view', 'id' => $id]);
        } else {
            Yii::$app->session->setFlash('error', 'Some error occured, please try agian.');
            $this->redirect(['donation/preview', 'id' => $id]);
        }
    }

    public function actionDownload($id) {
        $content = $this->renderPartial('_donationPDF', [
            'donation' => $this->findModel(base64_decode($id)),
        ]);

        $header = $this->renderPartial('_donationPdfHeader', [
            'donation' => $this->findModel(base64_decode($id)),
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => 'Krajee Report Title'],
            'methods' => [
                'SetHeader' => [$header],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        $mpdf = $pdf->api;
        $mpdf->SetHeader($header);
        $mpdf->SetFooter('{PAGENO}');
        $mpdf->WriteHtml($content);
        $mpdf->Output('donation-' . base64_decode($id) . '.pdf', 'D');
    }

    public function actionAjaxGetEmployees() {
        if (Yii::$app->request->get('employeeCode') != '') {
            $result = Employees::findOne(['emp_id' => Yii::$app->request->get('employeeCode')]);
            $data['result'] = $result;
        } else {
            $data['result'] = "";
        }

        // return Json    
        return \yii\helpers\Json::encode($data);
    }

}

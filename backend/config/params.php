<?php
return [
    'adminEmail' => 'rahul.swami@ranosys.com',
    'siteUrl' => 'http://' . $_SERVER['HTTP_HOST'] . (strlen(dirname($_SERVER['SCRIPT_NAME'])) > 1 ? dirname($_SERVER['SCRIPT_NAME']) : '' ) . '/',
];

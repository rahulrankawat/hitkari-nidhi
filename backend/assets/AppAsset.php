<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $sourcePath = '@bower/';
    public $css = ['admin-lte/dist/css/AdminLTE.css', 'admin-lte/dist/css/skins/_all-skins.min.css'];
    public $js = ['admin-lte/dist/js/app.min.js', 'admin-lte/plugins/slimScroll/jquery.slimscroll.min.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string $emp_id
 * @property string $emp_name
 * @property string $school_name
 * @property int $district
 * @property int $block
 * @property string $contact_number
 * @property string $employee_email
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'emp_name', 'school_name', 'district', 'block', 'contact_number', 'employee_email'], 'required'],
            [['school_name'], 'string'],
            [['district', 'block'], 'integer'],
            [['emp_id', 'emp_name', 'employee_email'], 'string', 'max' => 255],
            [['contact_number'], 'string', 'max' => 50],
            [['emp_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emp_id' => 'Emp ID',
            'emp_name' => 'Emp Name',
            'school_name' => 'School Name',
            'district' => 'District',
            'block' => 'Block',
            'contact_number' => 'Contact Number',
            'employee_email' => 'Employee Email',
        ];
    }
}

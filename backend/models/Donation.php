<?php

namespace backend\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "donation".
 *
 * @property int $id
 * @property int $emp_id
 * @property int $ddo_id
 * @property string $emp_name
 * @property string $emp_school
 * @property string $donation_amount
 * @property string $contribution_year
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Donation extends \yii\db\ActiveRecord {

    public $start_date;
    public $end_date;
    public $year;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'donation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'emp_id', 'ddo_id', 'emp_name', 'emp_school', 'donation_amount', 'contribution_year', 'emp_email', 'draft_number', 'start_date', 'end_date', 'year'], 'required'],
            [['emp_school', 'status'], 'string'],
            [['donation_amount', 'emp_contact'], 'number'],
            [['emp_email'], 'email'],
            [['contribution_year', 'created_at', 'updated_at', 'description', 'mark_as_delete'], 'safe'],
            [['emp_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'emp_id' => 'Employee ID',
            'ddo_id' => 'DDO ID',
            'emp_name' => 'Employee Name',
            'emp_school' => 'Employee School',
            'emp_email' => 'Employee Email',
            'emp_contact' => 'Employee Contact Number',
            'donation_amount' => 'Donation Amount',
            'contribution_year' => 'Contribution Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Description',
            'draft_number' => 'Draft Number',
            'start_date' => 'Start Date',
            'end_date' => 'End Date'
        ];
    }

    public function search($params, $employeeCode, $districtId, $blockId) {
        $query = Donation::find()->where(['=', 'emp_id', $employeeCode])->orderBy(['created_at' => SORT_DESC]);
        if (($districtId != '' && $districtId != 'prompt') && ($blockId == 'prompt')) {
            $query = Donation::find()
                    ->leftJoin('employees', 'employees.emp_id=donation.emp_id')
                    ->where(['=', 'employees.district', $districtId])
                    ->orderBy(['donation.created_at' => SORT_DESC]);
        } else if (($blockId != '' && $blockId != 'prompt') && ($districtId != '' && $districtId != 'prompt') && ($districtId == 'prompt')) {
            $query = Donation::find()
                    ->leftJoin('employees', 'employees.emp_id=donation.emp_id')
                    ->where(['=', 'employees.district', $districtId])
                    ->andWhere(['=', 'employees.block', $blockId])
                    ->orderBy(['donation.created_at' => SORT_DESC]);
        } else if ($employeeCode != 'prompt') {
            $query = Donation::find()
                    ->leftJoin('employees', 'employees.emp_id=donation.emp_id')
                    ->where(['=', 'employees.emp_id', $employeeCode])
                    ->andWhere(['=', 'employees.district', $districtId])
                    ->andWhere(['=', 'employees.block', $blockId])
                    ->orderBy(['created_at' => SORT_DESC]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}

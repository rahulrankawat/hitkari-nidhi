<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "districts".
 *
 * @property int $id
 * @property string $district_name
 */
class Districts extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'districts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['district_name'], 'required'],
            [['district_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'district_name' => 'District Name',
        ];
    }

}

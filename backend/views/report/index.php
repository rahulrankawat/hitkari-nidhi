<?php
/* @var $this yii\web\View */

use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;

$this->title = 'Donation Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?= $this->title; ?></h1>
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
    ?>

    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generate report by days</h3>
                        <div class="box-body">
                            <div class="row">
                                <?php $form = ActiveForm::begin(['id' => 'donation-report-days']); ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=
                                        $form->field($model, 'start_date')->widget(
                                                DatePicker::className(), [
                                            'clientOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd-M-yyyy'
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=
                                        $form->field($model, 'end_date')->widget(
                                                DatePicker::className(), [
                                            'clientOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd-M-yyyy'
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <?= Html::submitButton('Generate', ['class' => 'btn btn-primary']) ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Generate report by year</h3>
                        <div class="box-body">
                            <div class="row">
                                <?php $form = ActiveForm::begin(['id' => 'donation-report-year']); ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= $form->field($model, 'year')->dropDownList(['prompt' => 'Select Year']); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<?php
/* @var $this yii\web\View */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
use kartik\typeahead\Typeahead;

$this->title = 'View Donation';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?= $this->title; ?></h1>
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
    ?>

    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Receipt No. RJHN-<?= $donation->id; ?></h3>
                        <div class="pull-right">
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/update?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-maroon btn-sm">Edit</a>
                            </div>
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/delete?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-navy btn-sm">Delete</a>
                            </div>
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/preview?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-olive btn-sm">Preview</a>
                            </div>
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/download?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-purple btn-sm">Download</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-body margin-top-receipt">
                        <dl class="dl-horizontal">
                            <?php //echo "<pre>"; print_r($donation); exit; ?>
                            <dt>Employee Name</dt>
                            <dd><?= $donation->emp_name; ?></dd>
                            <dt>Employee School</dt>
                            <dd><?= $donation->emp_school; ?></dd>
                            <dt>Employee Email</dt>
                            <dd><?= $donation->emp_email; ?></dd>
                            <dt>Employee Contact No.</dt>
                            <dd><?= $donation->emp_contact; ?></dd>
                            <dt>Employee ID</dt>
                            <dd><?= $donation->emp_id; ?></dd>
                            <dt>DDO ID</dt>
                            <dd><?= $donation->ddo_id; ?></dd>
                            <dt>Donation Amount</dt>
                            <dd><?= $donation->donation_amount; ?></dd>
                            <dt>Contribution Year</dt>
                            <dd><?= date('Y', strtotime($donation->contribution_year)); ?></dd>
                            <dt>Draft Number</dt>
                            <dd><?= $donation->draft_number; ?></dd>
                            <dt>Donation Date</dt>
                            <dd><?= date('d-M-Y', strtotime($donation->created_at)); ?></dd>
                            <dt>Email Status</dt>
                            <dd><?= ($donation->status == 1 ? '<span class="label label-success">Sent</span>' : '<span class="label label-danger">Not send</span>'); ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
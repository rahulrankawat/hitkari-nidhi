<html>
    <body>
        <div style="width:100%">
            <div style="width: 100%;">
                <div style="width: 100%; margin-top: 40px;">
                    <div style="width: 100%; text-align: center;">
                        <h4 style="text-decoration: underline;"><strong>Receipt of Donation</strong></h4>
                    </div>
                    <div style="margin-top: 40px;">
                        <div>
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <th style="width:30%; text-align: left; padding: 10px;">Employee Name</th>
                                        <td style="float: left;margin-left: 20%;padding: 10px;"><?= $donation->emp_name; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%; text-align: left; padding: 10px;">Employee School/Office</th>
                                        <td style="float: left;margin-left: 20%;padding: 10px;"><?= $donation->emp_school; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width:30%; text-align: left; padding: 10px;">Employee Email</th>
                                        <td style="float: left;margin-left: 20%;padding: 10px;"><?= $donation->emp_email; ?></td>
                                    </tr>
                                </tbody>
                            </table>

                            <table style="margin-top: 30px; border: 1px solid #000; width: 100%; border-collapse:collapse;">
                                <tbody>
                                    <tr>
                                        <th style="width: 20%; border: 1px solid #000; padding: 10px;">Employee ID</th>
                                        <th style="width: 15%; border: 1px solid #000; padding: 10px;">DDO ID</th>
                                        <th style="width: 10%; border: 1px solid #000; padding: 10px;">Amount</th>
                                        <th style="width: 25%; border: 1px solid #000; padding: 10px;">Contribution Year</th>
                                        <th style="width: 15%; border: 1px solid #000; padding: 10px;">Date</th>
                                        <th style="width: 15%; border: 1px solid #000; padding: 10px;">Draft #</th>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #000; padding: 10px; text-align: center;"><?= $donation->emp_id; ?></td>
                                        <td style="border: 1px solid #000; padding: 10px; text-align: center;"><?= $donation->ddo_id; ?></td>
                                        <td style="border: 1px solid #000; padding: 10px; text-align: center;"><?= $donation->donation_amount; ?></td>
                                        <td style="border: 1px solid #000; padding: 10px; text-align: center;"><?= date('Y', strtotime($donation->contribution_year)); ?></td>
                                        <td style="border: 1px solid #000; padding: 10px; text-align: center;"><?= date('d-M-Y', strtotime($donation->created_at)); ?></td>
                                        <td style="border: 1px solid #000; padding: 10px; text-align: center;"><?= $donation->draft_number; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>
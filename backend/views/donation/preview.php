<?php
/* @var $this yii\web\View */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
use kartik\typeahead\Typeahead;

$this->title = 'Donation Receipt';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?= $this->title; ?></h1>
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
    ?>

    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Receipt No. RJHN-<?= $donation->id; ?></h3>
                        <div class="pull-right">
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/update?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-maroon btn-sm">Edit</a>
                            </div>
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/view?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-purple btn-sm">View</a>
                            </div>
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/delete?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-navy btn-sm">Delete</a>
                            </div>
                            <div class="btn-group">
                                <a href="<?= Yii::$app->homeUrl . 'donation/send-email?id=' . base64_encode($donation->id) . ''; ?>" class="btn bg-olive btn-sm">Send Email</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-body receipt-padding">
                        <div class="col-md-11 text-center col-xs-12">
                            <h3 class="remove-top-m">HITKARI NIDHI<br>Department of Secondary Education<br>Bikaner (Rajasthan)</h3>
                        </div>
                        <div class="col-md-1 col-xs-12">
                            <img class="receipt-logo" src="<?= Yii::$app->params['siteUrl'] ?>images/receipt-logo.png"/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-11 text-center col-xs-12">
                            <h3><strong>Receipt of Donation</strong></h3>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 margin-top-receipt">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th style="width:30%">Employee Name</th>
                                            <td><?= $donation->emp_name; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Employee School/Office</th>
                                            <td><?= $donation->emp_school; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Employee Email</th>
                                            <td><?= $donation->emp_email; ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Employee ID</th>
                                            <th>DDO ID</th>
                                            <th>Amount</th>
                                            <th>Contribution Year</th>
                                            <th>Date</th>
                                            <th>Draft #</th>
                                        </tr>
                                        <tr>
                                            <td><?= $donation->emp_id; ?></td>
                                            <td><?= $donation->ddo_id; ?></td>
                                            <td><?= $donation->donation_amount; ?></td>
                                            <td><?= $donation->contribution_year; ?></td>
                                            <td><?= date('d-M-Y', strtotime($donation->created_at)); ?></td>
                                            <td><?= $donation->draft_number; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
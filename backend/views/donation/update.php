<?php
/* @var $this yii\web\View */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
use kartik\typeahead\Typeahead;

$this->title = 'Update Donation';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?= $this->title; ?></h1>
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);
    ?>

    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Donation Form</h3>
                        <div class="box-body">
                            <?php $form = ActiveForm::begin(['id' => 'add-donation']); ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <?=
                                        $form->field($model, 'emp_id')->widget(Typeahead::classname(), [
                                            'dataset' => [
                                                [
                                                    'local' => $employeeArray,
                                                    'limit' => 10
                                                ]
                                            ],
                                            'pluginOptions' => ['highlight' => true],
                                            'options' => ['placeholder' => 'Please Enter Employee ID', 'id' => 'employee_id'],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?=
                                        $form->field($model, 'ddo_id')->widget(Typeahead::classname(), [
                                            'dataset' => [
                                                [
                                                    'local' => $ddoArray,
                                                    'limit' => 10
                                                ]
                                            ],
                                            'pluginOptions' => ['highlight' => true],
                                            'options' => ['placeholder' => 'Please Enter DDO ID', 'id' => 'ddo_id'],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?=
                                        $form->field($model, 'emp_name')->widget(Typeahead::classname(), [
                                            'dataset' => [
                                                [
                                                    'local' => $empNameArray,
                                                    'limit' => 10
                                                ]
                                            ],
                                            'pluginOptions' => ['highlight' => true],
                                            'options' => ['placeholder' => 'Please Enter Employee Name', 'id' => 'employee_name'],
                                        ]);
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?=
                                        $form->field($model, 'emp_school')->widget(Typeahead::classname(), [
                                            'dataset' => [
                                                [
                                                    'local' => $schoolArray,
                                                    'limit' => 10
                                                ]
                                            ],
                                            'pluginOptions' => ['highlight' => true],
                                            'options' => ['placeholder' => 'Please Enter School Name', 'id' => 'school_name'],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?=
                                    $form->field($model, 'emp_email')->widget(Typeahead::classname(), [
                                        'dataset' => [
                                            [
                                                'local' => $empNameArray,
                                                'limit' => 10
                                            ]
                                        ],
                                        'pluginOptions' => ['highlight' => true],
                                        'options' => ['placeholder' => 'Please Enter Employee Email', 'id' => 'employee_email'],
                                    ]);
                                    ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'emp_contact')->textInput(['placeholder' => 'Please Enter Employee Contact No.', 'id' => 'employee_contact']) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'donation_amount')->textInput(['placeholder' => 'Please Enter Donation Amount', 'id' => 'donation_amount'])
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'draft_number')->textInput(['placeholder' => 'Please Enter Draft Number', 'id' => 'draft_number'])
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'description')->textarea(['placeholder' => 'Please Enter Description, If any....', 'id' => 'description', 'rows' => 4, 'cols' => 5])
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php $model->contribution_year = date('d-M-Y', strtotime($model->contribution_year)); ?>
                                        <?=
                                        $form->field($model, 'contribution_year')->widget(
                                                DatePicker::className(), [
                                            'clientOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd-M-yyyy'
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?= Html::submitButton('Update', ['class' => 'btn btn-primary pull-right']) ?>
                            <!-- /.row -->
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
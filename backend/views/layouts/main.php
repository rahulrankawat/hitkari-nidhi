<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <?php $this->head() ?>
        <link rel="stylesheet" type="text/css" href="<?= Yii::$app->params['siteUrl'] ?>css/style.css">
        <link rel="shortcut icon" href="<?= Yii::$app->params['siteUrl'] ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= Yii::$app->params['siteUrl'] ?>/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?= Yii::$app->params['siteUrl'] ?>css/error.css">
    </head>
    <body class="hold-transition login-page skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <?php $this->endBody() ?>
        <div class="wrap">
            <?php if (!Yii::$app->user->isGuest) { ?>
                <header class="main-header">
                    <a href="<?= Yii::$app->homeUrl ?>" class="logo">
                        <span class="logo-lg"><b>Hitkari Nidhi</b></span>
                    </a>

                    <nav class="navbar navbar-static-top">
                        <a href ="#" class="sidebar-toggle" data-toggle ="offcanvas" role ="button">
                            <span class ="sr-only">Toggle navigation</span>
                        </a>
                        <!--Navbar Right Menu -->
                        <div class = "navbar-custom-menu">
                            <ul class = "nav navbar-nav">
                                <li class = "dropdown user user-menu">
                                    <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">
                                        <img src ="<?= Yii::$app->params['siteUrl'] ?>images/site-logo.png" class ="user-image" alt ="User Image">
                                        <span class = "hidden-xs">Admin</span>
                                    </a>
                                    <ul class = "dropdown-menu">
                                        <li class = "user-header">
                                            <img src = "<?= Yii::$app->params['siteUrl'] ?>images/site-logo.png" class = "" alt ="User Image">
                                        </li>
                                        <li class = "user-footer">
                                            <div class = "pull-right">
                                                <?php
                                                echo Html::beginForm(['/site/logout'], 'post');
                                                echo Html::submitButton('Logout', ['class' => 'btn btn-default btn-flat']);
                                                echo Html::endForm();
                                                ?>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>

                <aside class="main-sidebar">
                    <section class="sidebar" style="height: auto;">
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="<?= Yii::$app->params['siteUrl'] ?>images/small-site-logo" class="img-circle" alt="User Image">
                            </div>
                            <div class="pull-left info">
                                <p>Administrator</p>
                                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                            </div>
                        </div>
                        <!-- sidebar menu: : style can be found in sidebar.less -->
                        <ul class="sidebar-menu tree" data-widget="tree">
                            <li class="treeview <?= ((Yii::$app->controller->id == 'site') ? 'active' : '') ?>">
                                <a href="<?= Yii::$app->homeUrl ?>">
                                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="treeview <?= ((Yii::$app->controller->id == 'donation') ? 'active' : '') ?>">
                                <a href="<?= Yii::$app->homeUrl . 'donation/index'; ?>">
                                    <i class="fa fa-money"></i> <span>Add Donation</span>
                                </a>
                            </li>
                            <li class="treeview <?= ((Yii::$app->controller->id == 'report') ? 'active' : '') ?>">
                                <a href="<?= Yii::$app->homeUrl . 'report/index'; ?>">
                                    <i class="fa fa-files-o"></i>
                                    <span>Donation Report</span>
                                </a>
                            </li>
                        </ul>
                    </section>
                </aside>
                <?php
            }
            ?>
            <?php
            if (Yii::$app->user->isGuest) {
                echo '<div class="container">';
            } else {
                echo '<div class="content-wrapper" style="min-height: 960px;">';
            }
            ?>
            <div class="loader" style="display: none;"></div>
            <?= $content ?>
            <?php
            if (!Yii::$app->user->isGuest) {
                echo Alert::widget();
            }
            ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
                <footer class="footer">
                    <div class="container text-center">
                        <p class="">Copyright &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
                    </div>
                </footer>
            <?php } ?>
        </div>


        
    </body>
    <script src="<?= Yii::$app->params['siteUrl'] ?>js/custom.js"></script>
</html>
<?php $this->endPage() ?>

<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
?>

<div class="login-box">
    <div class="login-logo">
        <img class="site-logo"src="<?= Yii::$app->params['siteUrl'] ?>images/site-logo.png"/>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Please fill out your email. A link to reset password will be sent there.</p>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

        <div class="form-group has-feedback">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Email Address'])->label(FALSE); ?>
            <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
        </div>


        <div class="row">
            <div class="col-xs-8"></div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
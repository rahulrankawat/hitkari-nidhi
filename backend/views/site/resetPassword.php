<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';

?>

<div class="login-box">
    <div class="login-logo">
        <img class="site-logo"src="<?= Yii::$app->params['siteUrl'] ?>images/site-logo.png"/>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Please choose your new password:</p>
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

        <div class="form-group has-feedback">
            <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => 'Password'])->label(FALSE) ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>


        <div class="row">
            <div class="col-xs-8"></div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
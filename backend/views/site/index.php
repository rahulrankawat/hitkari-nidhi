<?php
/* @var $this yii\web\View */

use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
    <h1><?= $this->title; ?></h1>
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]);

    /* ------ Get Districts Data----- */
    $districtData = ArrayHelper::map(\backend\models\Districts::find()->asArray()->all(), 'id', 'district_name');
//    echo "<pre>";
//    print_r($districtData);
//    exit;
    ?>

    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search Donations</h3>
                        <div class="box-body">
                            <div class="row">
                                <?php $form = ActiveForm::begin(['id' => 'filter-donation']); ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php
                                        if (isset($_POST['Districts'])) {
                                            $district->id = $_POST['Districts']['id'];
                                        }
                                        ?>
                                        <?= $form->field($district, 'id')->dropDownList($districtData, ['prompt' => 'Select District'])->label('District'); ?>
                                        <?= Html::hiddenInput('block_url', \Yii::$app->getUrlManager()->createUrl('site/ajax-get-blocks'), ['id' => 'block-url']) ?>
                                        <?= Html::hiddenInput('employee_url', \Yii::$app->getUrlManager()->createUrl('site/ajax-get-employees'), ['id' => 'ajax-employee-url']) ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= $form->field($block, 'id')->dropDownList(['prompt' => 'Select Block'])->label('Block'); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= $form->field($employess, 'id')->dropDownList(['prompt' => 'Select Employee'])->label('Employees'); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary dashboard-button']) ?>
                                </div>
                            </div>
                            <!-- /.row -->
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?=
                DataTables::widget([
                    'dataProvider' => $dataProvider,
                    'clientOptions' => [
                        "responsive" => true,
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'emp_name',
                        'emp_school',
                        'donation_amount',
                        'contribution_year',
                        ['class' => 'yii\grid\ActionColumn',
                            'header' => 'Actions',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('app', 'view'),
                                        'target' => '_blank',
                            ]);
                        },
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = 'donation/view?id=' . base64_encode($model->id);
                            return $url;
                        }
                    }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </section>
</section>
<?php if (isset($_POST['Districts'])) { ?>
    <script>
        $(document).ready(function() {
            $("#districts-id").trigger("change");
            $("#blocks-id option[value='<?php echo $_POST['Blocks']['id']; ?>']").attr("selected", "selected");
        });
    </script>
<?php }
?>

/*----- Dashboard JS changes------------*/
$("#districts-id").change(function() {
    var blockUrl;
    var districtId;
    var returnedData;
    districtId = $(this).val();
    blockUrl = $("#block-url").val();
    $(".loader").show();
    $.ajax({
        url: blockUrl,
        type: 'POST',
        data: {districtId: districtId},
        success: function(data) {
            returnedData = JSON.parse(data);
            if (returnedData.result) {
                $('#blocks-id option').remove();
                $("#blocks-id").append($('<option></option>').val('prompt').html('Select Block'));
                $.each(returnedData.result, function(key, value) {
                    $("#blocks-id").append($('<option></option>').val(value.id).html(value.block_name));
                });
            }
        },
        error: function(data) {
            alert('Something went wrong, please try again.');
        }
    });
    $(".loader").hide();
});

$("#blocks-id").change(function() {
    var employeeUrl;
    var blockId;
    var districtId;
    var returnedData;
    blockId = $(this).val();
    districtId = $("#districts-id").val();
    employeeUrl = $("#ajax-employee-url").val();
    $(".loader").show();
    $.ajax({
        url: employeeUrl,
        type: 'GET',
        data: {districtId: districtId, blockId: blockId},
        success: function(data) {
            returnedData = JSON.parse(data);
            if (returnedData.result) {
                $('#employees-id option').remove();
                $("#employees-id").append($('<option></option>').val('prompt').html('Select Employee'));
                $.each(returnedData.result, function(key, value) {
                    $("#employees-id").append($('<option></option>').val(value.emp_id).html(value.emp_name));
                });
            }
        },
        error: function(data) {
            alert('Something went wrong, please try again.');
        }
    });
    $(".loader").hide();
});


/*------ Donation JS changes-----------*/
$("#employee_id").change(function() {
    var employeeUrl;
    var returnedData;
    var employeeCode;
    employeeCode = $(this).val();
    employeeUrl = $("#ajax_employee_url").val();
    $(".loader").show();
    $.ajax({
        url: employeeUrl,
        type: 'GET',
        data: {employeeCode: employeeCode},
        success: function(data) {
            returnedData = JSON.parse(data);
            if (returnedData.result) {
                $("#employee_name").val(returnedData.result.emp_name);
                $("#school_name").val(returnedData.result.school_name);
                $("#employee_email").val(returnedData.result.employee_email);
                $("#employee_contact").val(returnedData.result.contact_number);
            }
        },
        error: function(data) {
            alert('Something went wrong, please try again.');
        }
    });
    $(".loader").hide();
});